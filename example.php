<html>
<head>
	<title>ForumAvatar Example</title>
</head>

<body>
	<?php
		/**
		 * @function avatar
		 * @params user
		 * @type string
		 * @return string
		 */
		function avatar($user)
		{
			// Set the script URI
			$script_uri = 'http://example.com/ForumAvatar.php';

			// Use the information and return an image string
			$user_avatar = file_get_contents($script_uri . '?' . $user);
			return "<img src=\"{$user_avatar}\" alt=\"{$user}'s Avatar'\" title=\"{$user}'s Avatar'\" />";
		}

		// Set our user
		$user = 'example';

		// Print the users avatar string
		print avatar($user);
	?>
</body>
</html>