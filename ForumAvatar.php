<?php
/* ------------------------------------------------------------------------------
   
   IPB FORUM AVATAR by ALAN WYNN                            (v2) | Sat, 10 Dec 11
   
   https://bitbucket.org/djekl/ipb-forumavatar/

   ------------------------------------------------------------------------------
   
   Copyright (c) 2011/2012, djekl Developments
   
   Permission is hereby granted, free of charge, to any person
   obtaining a copy of this software and associated documentation
   files (the "Software"), to deal in the Software without
   restriction, including without limitation the rights to use,
   copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following
   conditions:
   
   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.
   
   ------------------------------------------------------------------------------ */


$path = "./"; //Path to your forum relative to this PHP file, only change if this file is not in your forum folder.
    
## DEFAULT AVATAR ##
$DefaultAvatar = "http://".$_SERVER["HTTP_HOST"].str_replace("avatar.php", "", $_SERVER["SCRIPT_NAME"])."User.png";

$UserName = addslashes(str_replace("%20", " ", $_SERVER['QUERY_STRING']));
	
$UsersID = UserID($UserName);
$UsersAvatar = AvatarUrl($UsersID);
	
echo $UsersAvatar;

function UserID($User){
global $path, $DefaultAvatar;
	
	@require $path."conf_global.php";
	
	@mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);
	@mysql_select_db($INFO['sql_database']);
	
	$result = @mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."members` WHERE `name` = '$User'");
	
	if($result == false){
		return "0";
	}
	
	$row = @mysql_fetch_array($result, MYSQL_ASSOC);# or die(mysql_error());
	
	$UserID = $row['member_id'];
	
	if($UserID == null or ""){
		$UserID = "00";
	}
	
	@mysql_free_result($result);
	return $UserID;
}
	
function validate_url($input) { 
		if (!stristr($input, 'http://')) { 
			return false; 
		} 
	return true; 
}
	
function AvatarUrl($User){
global $path, $DefaultAvatar;
	
	@require $path."conf_global.php";
	
	@mysql_connect($INFO['sql_host'], $INFO['sql_user'], $INFO['sql_pass']);# or die(mysql_error());
	@mysql_select_db($INFO['sql_database']);# or die(mysql_error());
	
	$profile_portal = @mysql_query("SELECT * FROM `".$INFO['sql_tbl_prefix']."profile_portal` WHERE `pp_member_id` = '".$User."'");# or die(mysql_error());	
	$pp_row = @mysql_fetch_array($profile_portal, MYSQL_ASSOC);# or die(mysql_error());

	$AvatarUrl = $pp_row['pp_main_photo'];

	if($AvatarUrl == null){
		$AvatarUrl = $DefaultAvatar;
	}else if(!validate_url($AvatarUrl)){
		$AvatarUrl = "http://".$_SERVER["HTTP_HOST"].str_replace("./", "/", $path)."uploads/".$AvatarUrl;
	}

	@mysql_free_result($pp_row);
	return $AvatarUrl;
}

?>
